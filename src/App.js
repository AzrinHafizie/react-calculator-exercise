import { useReducer } from "react";
import "./App.css";

import DigitButton from "./Button";
import OperationButton from "./Operation";

export const ACTION = {
  ADD_DIGIT: "add-digit",
  DELETE_DIGIT: "delete-digit",
  CLEAR: "clear",
  SET_OPERATION: "set-operation",
};

function reducer(state, { type, payload }) {
  switch (type) {
    case ACTION.ADD_DIGIT:
      if (state.overwrite) {
        return {
          ...state,
          current: payload.digit,
          overwrite: false,
        };
      }
      if (payload.digit === "0" && state.current === "0") {
        return state;
      }
      if (payload.digit === "." && state.current?.includes(".")) {
        return state;
      }

      return {
        ...state,
        current: `${state.current || ""}${payload.digit}`,
        result: evaluate({
          ...state,
          current: `${state.current || ""}${payload.digit}`,
        }),
      };
    case ACTION.SET_OPERATION:
      if (payload.operation === "=") {
        if (
          state.operation == null ||
          state.current == null ||
          state.operand == null
        ) {
          return state;
        }
        return {
          ...state,
          overwrite: true,
          result: evaluate(state),
        };
      }

      if (state.current === null && state.operand === null) {
        return state;
      }

      if (state.operand == null) {
        return {
          ...state,
          operation: payload.operation,
          operand: state.current,
          current: null,
        };
      }

      if (state.current == null) {
        return {
          ...state,
          operation: payload.operation,
        };
      }

      return {
        ...state,
        operand: evaluate(state),
        operation: payload.operation,
        current: null,
        result: evaluate(state),
      };
    case ACTION.CLEAR:
      return {
        result: "0",
      };
    case ACTION.DELETE_DIGIT:
      if (state.overwrite) {
        return {
          ...state,
          overwrite: false,
          current: null,
        };
      }
      if (state.current == null) {
        return {
          ...state,
          operation: null,
        };
      }
      if (state.current.length === 1) {
        return {
          ...state,
          current: null,
          result: evaluate({
            current: null,
            operand: state.operand,
            operation: state.operation,
          }),
        };
      }
      return {
        ...state,
        current: state.current.slice(0, -1),
        result: evaluate(state),
      };

    default:
      break;
  }
}

function evaluate({ current, operand, operation }) {
  console.log(current, operand, operation);
  const prev = parseFloat(operand);
  current = parseFloat(current);

  let computation = current;
  if (isNaN(current)) return (computation = operand);
  switch (operation) {
    case "+":
      computation = prev + current;
      break;
    case "-":
      computation = prev - current;
      break;
    case "×":
      computation = prev * current;
      break;
    case "÷":
      computation = prev / current;
      break;
    default:
      break;
  }
  return computation.toString();
}

const INTERGER_FORMATTER = new Intl.NumberFormat("en-us", {
  maximumFractionDigits: 0,
});

function formatOperand(operand) {
  if (operand == null) return;
  const [integer, decimal] = operand.split(".");
  if (decimal == null) return INTERGER_FORMATTER.format(integer);
  return `${INTERGER_FORMATTER.format(integer)}.${decimal}`;
}

function App() {
  const [{ current, operand, operation, result }, dispatch] = useReducer(
    reducer,
    {}
  );

  return (
    <div className="calculator-grid">
      <div className="display">
        <div className="operand">
          {formatOperand(operand)} {operation} {formatOperand(current)}
        </div>
        <div className="current">{formatOperand(result)}</div>
      </div>
      <button
        className="calculator-btn span-2"
        onClick={() => dispatch({ type: ACTION.DELETE_DIGIT })}
      >
        DEL
      </button>
      <button
        className="calculator-btn"
        onClick={() => dispatch({ type: ACTION.CLEAR })}
      >
        AC
      </button>
      <OperationButton operation="÷" dispatch={dispatch} />
      <DigitButton digit="7" dispatch={dispatch} />
      <DigitButton digit="8" dispatch={dispatch} />
      <DigitButton digit="9" dispatch={dispatch} />
      <OperationButton operation="×" dispatch={dispatch} />
      <DigitButton digit="4" dispatch={dispatch} />
      <DigitButton digit="5" dispatch={dispatch} />
      <DigitButton digit="6" dispatch={dispatch} />
      <OperationButton operation="-" dispatch={dispatch} />
      <DigitButton digit="1" dispatch={dispatch} />
      <DigitButton digit="2" dispatch={dispatch} />
      <DigitButton digit="3" dispatch={dispatch} />
      <OperationButton operation="+" dispatch={dispatch} />
      <DigitButton digit="0" dispatch={dispatch} />
      <DigitButton digit="." dispatch={dispatch} />
      <OperationButton operation="=" dispatch={dispatch} />
    </div>
  );
}

export default App;
