import { ACTION } from "./App";

export default function OperationButton({ dispatch, operation }) {
  return (
    <button
      className={
        operation === "="
          ? "calculator-btn btn-operation span-2"
          : "calculator-btn btn-operation"
      }
      onClick={() =>
        dispatch({ type: ACTION.SET_OPERATION, payload: { operation } })
      }
    >
      {operation}
    </button>
  );
}
